.. mandyoc-docs documentation master file, created by
   sphinx-quickstart on Mon Nov  2 14:45:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MANDYOC Documentation
=====================

Welcome to the MANDYOC Documentation page.

MANDYOc is short for **MANtle DYnamics simulatOr Code**. It is a numerical code
written on top of the `PETSc`_ library and it simulates thermochemical
convection of the Earth's mantle.

On this page, you will be able to find relevant information on how the code
works, how it was implemented, how to install it, the description of the
parameters and input files, some applications and some useful examples.

.. include:: files/readme.rst

.. toctree::
    :hidden:
    :numbered:
    :maxdepth: 2
    :caption: Getting started

    files/theory
    files/implementation
    files/installation
    files/parameter-file
    files/input
    files/benchmarks
    files/references

.. toctree::
    :hidden:
    :numbered:
    :maxdepth: 2
    :caption: Getting help and contributing

    How to contribute <https://bitbucket.org/victorsacek/mandyoc/src/master/CONTRIBUTING.md>
    Code of Conduct <https://bitbucket.org/victorsacek/mandyoc/src/master/CODE_OF_CONDUCT.md>
    Source code on Bitbucket <https://bitbucket.org/victorsacek/mandyoc/src/master>


.. _PETSc: https://www.mcs.anl.gov/petsc/

