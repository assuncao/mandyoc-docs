

Need any help?
--------------

If you need any help, send us an email to sacek@usp.br or jamison.assuncao@usp.br

Contributing
------------

Code of conduct
+++++++++++++++

Please note that this project is released with a `Contributor Code of
Conduct <https://bitbucket.org/victorsacek/mandyoc/src/master/CODE_OF_CONDUCT.md>`__.
By participating in this project you agree to abide by its terms.

Contributing Guidelines
+++++++++++++++++++++++

**Contributions to MANDYOC are welcome**. If you have an issue, a bug, a code
contribution or a documentation contribution, **thanks for helping to improve
MANDYOC!**
Check out our `Contributing guide
<https://bitbucket.org/victorsacek/mandyoc/src/master/CONTRIBUTING.md>`__.


License
-------

This is free software, you can redistribute it and/or modify it under the terms
of the **BSD 3-clause License**. A copy of this license is provided in
`LICENSE <https://bitbucket.org/victorsacek/mandyoc/src/master/LICENSE>`__.

